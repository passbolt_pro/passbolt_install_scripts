#!/usr/bin/env bash
#
# Small script that prepares install scripts for distribution
#
#
#
#

set -euo pipefail

build() {
  local output=dist/"$os"/passbolt_pro_"$os"_installer.sh

  mkdir -p dist/"$os"/conf/{nginx,php}
  {
  cat templates/header.in
  cat conf/constants_common.sh
  cat "conf/$os/constants.sh"
  } >> "$output"

  for util in lib/helpers/utils/*.sh; do
    cat "$util" >> "$output";
  done

  for validator in lib/validators/*.sh; do
    cat "$validator" >> "$output";
  done

  for validator in lib/validators/*.sh; do
    cat "$validator" >> "$output";
  done

  for initializer in lib/initializers/*.sh; do
    cat "$initializer" >> "$output";
  done

  if [ "$os" == "centos7" ] || [ "$os" == "centos8" ]|| [ "$os" == "redhat" ]; then
    for helper in lib/helpers/"$os"/*.sh; do
      cat "$helper" >> "$output";
    done
  fi

  for helper in lib/helpers/*.sh; do
    cat "$helper" >> "$output";
  done

  cat "lib/main/$os/main.sh" >> "$output"

  if [ "$os" == "redhat" ]; then
    sed -i s:-euo:-eo: "$output"
    sed -i s:/etc/nginx:/etc/opt/rh/rh-nginx116/nginx: "$output"
  fi

  chmod +x "$output"

  cp conf/nginx/*.conf "dist/$os/conf/nginx"
  cp conf/php/*.conf "dist/$os/conf/php"
  cp "conf/$os/packages.txt" "dist/$os/conf/packages.txt"
}

build_ssl_vm() {
  local output=dist/"$os"/passbolt_pro_"$os"_ssl_installer.sh

  {
    cat templates/header.in
    cat conf/constants_common.sh
    cat "conf/$os/constants.sh"
    echo "readonly INSTRUCTIONS_BASE_DIR='/var/www/instructions'"
    cat lib/helpers/utils/errors.sh
    cat lib/helpers/utils/messages.sh
    cat lib/validators/_hostname_validator.sh
    cat lib/validators/_os_permissions_validator.sh
    cat lib/validators/_ssl_paths_validator.sh
    cat lib/validators/_os_validator.sh
    cat lib/initializers/_interactive_option_ssl.sh
    cat lib/initializers/_interactive_option_hostname.sh
    cat lib/initializers/_variable_accessor.sh
    cat lib/initializers/init_config.sh
    cat lib/helpers/_setup_ssl.sh | sed s:PASSBOLT_BASE_DIR\"\/webroot:INSTRUCTIONS_BASE_DIR\":
    cat lib/helpers/setup_nginx.sh
    cat lib/helpers/service_enabler.sh
    cat lib/main/vm/main.sh
  } >> "$output"

  chmod +x "$output"
}

compress() {
  mkdir -p dist/tar/"$1"
  cd dist/"$1" || exit 1
  tar cvfz passbolt-pro-installer-"$1".tar.gz *
  cd -
  mv dist/"$1"/passbolt-pro-installer-"$1".tar.gz dist/tar/"$1"
}

checksum() {
  cd dist/tar/"$1" || exit 1
  sha512sum passbolt-pro-installer-"$1".tar.gz > SHA512SUMS.txt
}

os="$1"
case $os in
  'centos7')
    build
    compress centos7
    checksum centos7
    ;;
  'centos8')
    build
    compress centos8
    checksum centos8
    ;;
  'redhat')
    build
    compress redhat
    checksum redhat
    ;;
  'clean')
    rm -rf dist/*
    ;;
  *)
    echo "No such build option"
    exit 1
    ;;
esac
